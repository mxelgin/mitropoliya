
��������� �������
\.framework---------------����� � ������ ��� ���� ����� �������
\.framework\plugins-------swf, js ����� �������
\.framework\skin----------�����, ������������� ��� ����� ��������� ���������� � �������, ����, ������, ����� � �.�.
\.framework\sound---------�����, ��� �������� ������
\.framework\xml-----------�����, ��� xml ����������� ������
\.framework\xml\control_plugin.xml---�������� ���������� ����������� �����
\.framework\xml\function.xml---------������� ����, ������������ ��� ����������� ��������� xml ������
\.framework\xml\hint.xml-------------����������� ����� ��� ��������� �� ������� �����
\.framework\xml\info.xml-------------�������� ���������� ����������� �����, ��� ������� ������ "@" � ����
\.framework\xml\lng-select.xml-------�������� ���������� ��������
\.framework\xml\map.xml--------------�������� ���������� ������� ������
\.framework\xml\sound.xml------------���������� ���������� ������ � ������� ������
\.framework\xml\thumb.xml------------��������� ������������ ������� � ������� ������
\.framework\xml\vr.xml---------------�������� ���������� VR �����
\.framework\xml\minimap.xml----------���������,�����
\.framework\xml\theme.xml------------������������ �������
\.framework\xml\audiogid.xml---------���������� ����������

��������� ����
\{tour}\tour.xml----------������� ����, � ��� ������������ ��� �����
\{tour}\{scene}.xml---------���� �����, � ��� ��������������� ��������
\{tour}\exhibits----------�����, � �������� �������
\{tour}\exhibits\scene----����� �����, �������� ����� ������ ��������������� �������� �����
\{tour}\panos-------------�����
\{tour}\skin--------------�����, ������������� ��� ����� ��������� ���������� � ����, ����, ������, ��������� � �.�.
\{tour}\sound-------------�����, ��� �������� ������
\{tour}\xml---------------�����, ��� xml ����������� ������
\{tour}\xml\content.xml--------------����������, �����, ������, ���������� ����������
\{tour}\xml\function.xml-------------������� ����, ������������ ��� ����������� ��������� xml ������
\{tour}\xml\minimap_koord.xml--------------���������� �� ���������
\{tour}\xml\gid.xml------------------------������� ���������

������:
krpano 1.17
object2vr 2.0.2

����:
���������� ���� ��� ������ ����� � ������ ������� ������������ � ����� content.xml
����� ����������� 
<sound name="{locale}_sound_{scene}" text="{mp3}"/>
��������:
<sound name="ru_sound_scene__MG_5171_Panorama" text="sound/ru/01.mp3"/>
<sound name="en_sound_scene__MG_5171_Panorama" text="sound/en/01.mp3"/>
<sound name="fr_sound_scene__MG_5171_Panorama" text="sound/fr/01.mp3"/>
<sound name="ge_sound_scene__MG_5171_Panorama" text="sound/ge/01.mp3"/>


���.�������:

1.��� ��������� �������� �����������.
������ ����������� ���. ������� �� �������� ������ ����� ����������.
� ����� .framwork/xml/function.xml
���������� enabled="true"
	<autorotate enabled="false"
            waittime="3.0" 
            speed="3.0" 
            horizon="0.0" 
            tofov="120.0" 
            />

2.��� ��������� ������� ���������� (������������ ������, ������������ ������ � ����������, ������ � �.�.)

�������� ������ ���������� ����� � ����� .framwork/xml/control_plugin.xml
��� ������� ������������ � ������ �� ������� ondown/onup 
��� ������ ���������� �� ����������� � ������. 
bg_vr - �������� ������ ������ ��� 3� ���� �������
bg - �������� ������ ������ ��� ����������� �������
bg_info - �������� ������ ������ ������������� ������ ��� ����������� �������
bg_home - �������� ������ ������ ������������� ����� ��� ����������� �������
bg_menu - �������� ������ ������ ������������ ������ ������������� ��� ������� ���� ������ �����

����� menu_show() ���������� ������ ������ bg_vr, bg, bg_info, bg_home. �������� bg_menu
����� menu_hide() �������� ������ ������ bg_vr, bg, bg_info, bg_home. ���������� bg_menu

 

3.���������� �������� ������� (������������ ������, ������������ ������ � ����������, ������ � �.�.)

��������  �������� ������ ����� � ����� .framwork/xml/lng-select.xml
����� ����� ���������� � ������ show_tour();
����� ������ ����� ����������� ���������� ���������� check_locale ���������� � ���� �� ���������: ru,en,ge,fr
�������� ���:
action(check_locale,ru);

4.����������� ��������� � ����� �� ������� �����,����, 3D (� ��������� � ���� �� �������� ����� ��������� �� ����� ������, �����, ��� ����� �������� ������ � ��� ������� ����������, �� ��� � ���...).

��� ���������� ���������� ����������� ������ options.swf 
������ ����� ���������� � ������� ����� tour.xml
<plugin name="options" url="plugins/options.swf" keep="true"/>

��� ��������� ����� � �������� ���������� ������� ��������� ��������� startscene,hlookat,vlookat
��������:
/index.html?startscene=scene__MG_5189_Panorama&hlookat=-241,18

5.���������� ������� ����������� ��������� ����� � �����������.

����� ���������� � ����� ��������� minimap.xml
��� ������� ���� ������������ ���� ���������� ���������, 
���� �� ��������� \{tour}\xml\minimap.xml

��������, ���� �� ��������� � ���� 04_1vtour
\04_1vtour\xml\minimap.xml

��� ���� ����� ���������� ����������� ������ ��� ���� ���� ���������� �������� �������� radarhead
��������:
<scene radarhead="32.30" name="scene__MG_5171_Panorama" title="_MG_5171 Panorama" onstart="" thumburl="panos/_MG_5171_Panorama.tiles/thumb.jpg" lat="" lng="" heading="">

���������� ����� ����� � ������� ������� options.swf

6.����������
���������� ������������ ����� ������� <scene> � krpano
������������ ����� ���������������:
	    <view 
          hlookat       ="0" 
          vlookat       ="0" 
          camroll       ="0.0000" 
          fov           ="2.00" 
          fovmin        ="1.00" 
          fovmax        ="20.00" 
          fisheye       ="0" 
          fisheyefovlink="0.0" 
          limitfov      ="true" 
          limitview     ="range" 
          vlookatmin    ="-3" 
          vlookatmax    ="+3" 
          hlookatmax    ="+6" 
          hlookatmin    ="-6" 
        /> 			
��� ������ ���� CILINDER
������ ���������� �������� ����������� tiledimagewidth, tiledimageheight 		
		<image type="CYLINDER" hfov="1.00" multires="true" tilesize="612" frames="1" frame="1">
			<level tiledimagewidth="320" tiledimageheight="612">
				<cylinder url="panos/001.tiles/001.jpg" />
			</level>
		</image>
7.�D ���������� krpano
��� ��������� ��� �� ��� � � �.6
������� � ��� ��� ��� ���������� ��������� �� ��� ���������� ������ ����� %00f.jpg		
			<level tiledimagewidth="320" tiledimageheight="612">
				<cylinder url="panos/001.tiles/%00f.jpg" />
			</level>
8.3D ���������� object2vr
������������ skin mytest.ggsk ��� ����������� ������.

9. ��������� ���� ������� theme.xml
� ����� �������� ������������ ���������

10. audiogid
���������� ����� ������� ���� �������
<audiogid path="audiogid/ru" play="true"/>
��� 
path ����� �� mp3 ������
play - ��������� ����, true|false
� ����� gid.xml ������� ���� ����������� ������� ������ ����
<gid>
	<scene name="scene_01_sphere" next="scene_02_sphere"/>
</gid>
���, name - �������� �������� ����
next - �������� ���������� ����
��� ����� ��������� ��� ��������� �������� ���� ��� next ����������, 
��� � ����� ���� ���� next �����������